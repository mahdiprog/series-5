package sbu.cs;

public interface WhiteFunctionsInterface {
    /**
     * number 1 function
     *
     * @param arg1     first input string for mixing 2 string
     * @param arg2     second input string for mixing 2 string
     * @return the output string of function
     */
    public String mix(String arg1, String arg2);

    /**
     * number 2 function
     *
     * @param arg1     first input string for combine with reversed arg2
     * @param arg2     second input string for reverse and combine with arg1
     * @return the output string of function
     */
    public String reverseMerge(String arg1, String arg2);

    /**
     * number 3 function
     *
     * @param arg1     first input string for mix with reversed arg2
     * @param arg2     second input string for reverse and mix with arg2
     * @return the output string of function
     */
    public String reverseMix(String arg1, String arg2);

    /**
     * number 4 function
     *
     * @param arg1     first input string for checking length is even or not
     * @param arg2     second input string
     * @return the output string of function
     */
    public String IsLengthFirstStrEven(String arg1, String arg2);

    /**
     * number 5 function
     *
     * @param arg1     first input string for sum chars with arg2 chars and do %26
     * @param arg2     second input string for sum chars with arg1 chars and do %26
     * @return the output string of function
     */
    public String sumChars(String arg1, String arg2);
}
