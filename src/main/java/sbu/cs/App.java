package sbu.cs;

public class App {

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input) {
        Home[][] board = new Home[n][n];
        Home first = new First(input);

        final int rows = n;
        final int columns = n;

        //initialize homes by type
        for (int i = 0 ; i < rows ; i++) {
            for (int j = 0; j < columns ; j++) {
                if(i == 0 || j == 0){
                    //green
                    if((i == 0 && j != columns - 1) || (i != columns - 1  && j == 0)){
                        if(i == 0 && j == 0){
                            board[i][j] = new GreenHome(first);
                        }
                        else if(i == 0) {
                            board[i][j] = new GreenHome(board[i][j-1]);
                        }
                        else if(j == 0){
                            board[i][j] = new GreenHome(board[i-1][j]);
                        }
                    }
                    else{
                        //yellow
                        if(i==0) {
                            board[i][j] = new YellowHome(board[i][j-1]);
                        }
                        else{
                            board[i][j] = new YellowHome(board[i - 1][j]);
                        }
                    }
                }
                else if(i == columns - 1 || j == columns - 1){
                    //pink
                    if(i == columns - 1 && j != columns - 1){
                        board[i][j] = new PinkHome(board[i][j-1], board[i-1][j]);
                    }
                    else if(i != columns - 1 && j == columns - 1) {
                        board[i][j] = new PinkHome(board[i][j-1], board[i-1][j]);
                    }
                    else {
                        board[i][j] = new PinkHome(board[i][j-1], board[i-1][j]);
                    }
                }
                else{
                    //blue
                    board[i][j] = new BlueHome(board[i][j-1],board[i-1][j]);
                }
            }
        }

        //going into board and doing functions of home
        for (int i = 0 ; i < rows ; i++) {
            for (int j = 0; j < columns; j++) {
                board[i][j].operation(arr[i][j]);
            }
        }
        return board[n-1][n-1].getOutput1();
    }
}
