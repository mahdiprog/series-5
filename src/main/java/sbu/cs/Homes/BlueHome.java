package sbu.cs;

public class BlueHome extends Home{
    public BlueHome (Home input1 ,Home input2){
        this.first_input = input1;
        this.second_input = input2;
        this.type = "Blue";
    }
}
