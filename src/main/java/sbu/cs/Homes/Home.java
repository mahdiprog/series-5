package sbu.cs;

public class Home {
    protected Home first_input;
    protected Home second_input;
    protected String output1;
    protected String output2;
    protected String type;

    //recognize the type of homes and call correctly functions
    void operation(int choice){
        if(type.equals("Green")){
            output1 = blackOperation(choice, first_input.getOutput1());
        }
        else if(type.equals("Blue")) {
            output1 = blackOperation(choice, first_input.getOutput1());
            output2 = blackOperation(choice, second_input.getOutput2());
        }
        else if(type.equals("Yellow")){
            output1 = blackOperation(choice, first_input.getOutput1());
        }
        else if(type.equals("Pink")){
            output1 = whiteOperation(choice, first_input.getOutput1(), second_input.getOutput2());
        }
    }

    //call the correct choice for black functions
    String blackOperation(int choice , String input){
        BlackFunctions operate = new BlackFunctions();
        switch (choice){
            case 1:
                return operate.reverse(input);
            case 2:
                return operate.repeatChar(input);
            case 3:
                return operate.repeatStr(input);
            case 4:
                return operate.shift(input);
            case 5:
                return operate.reverseAlphabet(input);
            default:
                return null;
        }
    }

    //like blackfunctions
    String whiteOperation(int choice , String input1 , String input2){
        WhiteFunctions operate = new WhiteFunctions();
        switch (choice){
            case 1:
                return operate.mix(input1 , input2);
            case 2:
                return operate.reverseMerge(input1 , input2);
            case 3:
                return operate.reverseMix(input1 , input2);
            case 4:
                return operate.IsLengthFirstStrEven(input1 , input2);
            case 5:
                return operate.sumChars(input1 , input2);
            default:
                return null;
        }
    }

    public String getOutput1() {
        return output1;
    }

    public String getOutput2() {
        if(output2 != null) {
            return output2;
        }
        return output1;
    }
}
