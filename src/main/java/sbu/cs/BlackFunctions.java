package sbu.cs;

public class BlackFunctions implements  BlackFunctionsInterface{
    @Override
    public String reverse(String arg) {
        String result = "";
        char ch;
        for (int i = 0 ; i < arg.length() ; i++){
            ch = arg.charAt(i);
            result = ch + result;
        }
        return result;
    }

    @Override
    public String repeatChar(String arg) {
        String result = "";
        char ch;
        for (int i = 0 ; i < arg.length() ; i++){
            ch = arg.charAt(i);
            result = result + ch + ch;
        }
        return result;
    }

    @Override
    public String repeatStr(String arg) {
        return (arg + arg);
    }

    @Override
    public String shift(String arg) {
        String result = arg.charAt(arg.length() - 1) + arg;
        result = result.substring(0 , result.length() - 1);
        return result;
    }

    @Override
    public String reverseAlphabet(String arg) {
        String result = "";
        char ch;
        for (int i = 0 ; i < arg.length() ; i++){
            ch = arg.charAt(i);
            if(Character.isUpperCase(ch)){
                ch = (char)(90 - ((int)ch - 65));
                result = result + ch;
            }
            else{
                ch = (char)(122 - ((int)ch - 97));
                result = result + ch;
            }
        }
        return result;
    }
}
