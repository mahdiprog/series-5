package sbu.cs;

public class WhiteFunctions implements WhiteFunctionsInterface{
    @Override
    public String mix(String arg1, String arg2) {
        String result = "";
        int i;
        for (i = 0 ; i < 2 * arg1.length() && i < 2 * arg2.length() ; i++){
            if(i % 2 == 0){
                result = result + arg1.charAt(i/2);
            }
            else {
                result = result + arg2.charAt(i/2);
            }
        }
        if (i < 2 * arg1.length()){
            result += arg1.substring(i);
        }
        else if(i < 2 * arg2.length()){
            result += arg2.substring(i);
        }
        return result;
    }

    @Override
    public String reverseMerge(String arg1, String arg2) {
        StringBuilder sb=new StringBuilder(arg2);
        sb.reverse();
        return arg1 + sb.toString();
    }

    @Override
    public String reverseMix(String arg1, String arg2) {
        StringBuilder sb=new StringBuilder(arg2);
        sb.reverse();
        arg2 = sb.toString();
        String result = "";
        int i;
        for (i = 0 ; i < 2 * arg1.length() && i < 2 * arg2.length() ; i++){
            if(i % 2 == 0){
                result = result + arg1.charAt(i/2);
            }
            else {
                result = result + arg2.charAt(i/2);
            }
        }
        if (i < 2 * arg1.length()){
            result += arg1.substring(i);
        }
        else if(i < 2 * arg2.length()){
            result += arg2.substring(i);
        }
        return result;
    }

    @Override
    public String IsLengthFirstStrEven(String arg1, String arg2) {
        if(arg1.length() %2 == 0){
            return arg1;
        }
        return arg2;
    }

    @Override
    public String sumChars(String arg1, String arg2) {
        String result = "";
        int i;
        for (i = 0 ; i < arg1.length() && i < arg2.length() ; i++){
            int letter1 = (int)arg1.charAt(i) - (int)('a');
            int letter2 = (int)arg2.charAt(i) - (int)('a');
            int temp = (letter1 + letter2) % 26;
            result += (char)(temp + (int)('a'));
        }
        if (i < arg1.length()){
            result += arg1.substring(i);
        }
        else if(i < arg2.length()){
            result += arg2.substring(i);
        }
        return result;
    }
}
