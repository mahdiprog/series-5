package sbu.cs;

public interface BlackFunctionsInterface {
    /**
     * number 1 function
     *
     * @param arg     input string for make reverse
     * @return the output string of function
     */
    public String reverse(String arg);

    /**
     * number 2 function
     *
     * @param arg     input string for put double char
     * @return the output string of function
     */
    public String repeatChar(String arg);

    /**
     * number 3 function
     *
     * @param arg     input string for put double string
     * @return the output string of function
     */
    public String repeatStr(String arg);

    /**
     * number 4 function
     *
     * @param arg     input string for shift to right
     * @return the output string of function
     */
    public String shift(String arg);

    /**
     * number 5 function
     *
     * @param arg     input string for make reverse chars from alphabet
     * @return the output string of function
     */
    public String reverseAlphabet(String arg);
}
