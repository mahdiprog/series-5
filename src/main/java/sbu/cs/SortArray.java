package sbu.cs;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {
        for (int i = 0 ; i < size ; i++){
            for (int j = 0 ; j < size ; j++){
                if(arr[i] < arr[j]){
                    int temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                }
            }
        }
        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        for (int i = 0 ; i < size ; i++){
            for (int j = i - 1 ; j >= 0 ; j--){
                if(arr[i] < arr[j]){
                    int temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                    i--;
                }
            }
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        if(size == 1){
            return arr;
        }
        int first_size = arr.length / 2;
        int second_size = arr.length - first_size;
        int [] first = new int[first_size];
        int [] second = new int[second_size];
        System.arraycopy(arr, 0, first, 0, first.length);
        System.arraycopy(arr, first.length, second, 0, second.length);

        mergeSort(first,first_size);
        mergeSort(second,second_size);

        int n = 0 , i = 0 , j = 0;
        while (i<first_size && j<second_size){

            if(first[i]<second[j]){
                arr[n++] = first[i++];
            }
            else{
                arr[n++] = second[j++];
            }
        }
        while(i<first_size){
            arr[n++] = first[i++];
        }
        while(j<second_size){
            arr[n++] = second[j++];
        }
        return arr;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int mid = arr.length / 2;
        while (mid != 0 && mid < arr.length) {
            if (arr[mid] > value) {
                mid /= 2;
            } else if (arr[mid] < value) {
                mid = mid + mid / 2;
            } else if (arr[mid] == value) {
                return mid;
            }
        }
        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        int mid = arr.length / 2;
        int pos = 0;
        int[] temp;
        if(arr.length == 1){
            if(arr[0] == value){
                return 0;
            }
            return -1;
        }
        if (arr[mid] > value) {
            temp = new int[mid];
            System.arraycopy(arr, 0, temp, 0, temp.length);
            pos = binarySearchRecursive(temp , value);
        }
        else if (arr[mid] < value) {
            temp = new int[mid];
            System.arraycopy(arr, mid, temp, 0, temp.length);
            pos = binarySearchRecursive(temp , value) + mid;
        }
        else {
            pos = mid;
        }
        if(pos == -1 || pos == -1 + mid){
            return -1;
        }
        return pos;
    }
}
